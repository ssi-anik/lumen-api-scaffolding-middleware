<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Exception;

class HealthController extends ApiController
{
    public function ping () {
        return $this->respondSuccess([
            'error'   => false,
            'message' => 'Pong!',
            'time'    => Carbon::now()->toDateTimeString(),
        ]);
    }

    public function health () {
        $causes = [];

        $cache = null;
        try {
            app('cache')->connection();
        } catch ( Exception $e ) {
            $causes[] = 'Cache';
        }

        $database = null;
        try {
            app('db')->connection()->getPdo();
        } catch ( Exception $e ) {
            $causes[] = 'Database';
        }

        return response()->json([
            'error'   => empty($causes) ? false : true,
            'message' => !empty($causes) ? 'I am feeling down!' : 'Running healthy <3',
            'causes'  => $causes,
        ], empty($causes) ? 200 : 503);
    }
}
