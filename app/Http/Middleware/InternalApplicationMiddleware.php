<?php

namespace App\Http\Middleware;

use Closure;

class InternalApplicationMiddleware
{
    public function handle ($request, Closure $next, ...$parameters) {
        $handler = $parameters[0];
        $environments = array_filter(array_slice($parameters, 1));

        if ($environments && !in_environment($environments)) {
            return abort(401, 'Unauthorized to access this URL.');
        }

        $allowedApiKey = config('settings.api_keys.' . $handler);
        $providedApiKey = $request->header('x-api-key');

        if (!$allowedApiKey || $allowedApiKey != $providedApiKey) {
            return abort(401, 'Unauthorized to access this URL.');
        }

        return $next($request);
    }
}
