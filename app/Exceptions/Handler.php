<?php

namespace App\Exceptions;

use Anik\Form\ValidationException as FormValidationException;
use App\Http\Controllers\Api\ApiController;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    protected $dontReport = [
        AuthorizationException::class,
        UnauthorizedException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
        FormValidationException::class,
        GenericException::class,
    ];

    public function report (Exception $exception) {
        if (report_to_sentry() && app()->bound('sentry') && $this->shouldReport($exception)) {
            app('sentry')->captureException($exception);
        }

        parent::report($exception);
    }

    public function render ($request, Exception $exception) {

        switch ( true ) {
            case $exception instanceof UnauthorizedException:
                return $this->responder([
                    'error'   => true,
                    'message' => $exception->getMessage() ?: 'Unauthorized.',
                ], 401);
            case $exception instanceof QueryException:
                return $this->responder([
                    'error'   => true,
                    'message' => 'Something went wrong.',
                ], 500);
            case $exception instanceof FormValidationException:
            case $exception instanceof ValidationException:
                return $this->parseValidationErrorResponse($exception);
            case $exception instanceof ModelNotFoundException:
            case $exception instanceof NotFoundHttpException:
                return $this->responder([
                    'error'   => true,
                    'message' => 'Resource not available.',
                ], 404);
            case $exception instanceof BaseException:
                $data = $exception->getData();
                $statusCode = $exception->getHttpStatusCode();

                return $this->responder((!empty($data)
                    ? $data
                    : [
                        'error'   => true,
                        'message' => $exception->getResponseMessage(),
                    ]), $statusCode);
            case $exception instanceof MethodNotAllowedHttpException:
                $message = 'Method now allowed.';
            case $exception instanceof HttpException:
                $statusCode = $exception->getStatusCode();
                $message = $message ?? $exception->getMessage();
                $headers = $exception->getHeaders() ?: [];

                return $this->responder([
                    'error'   => true,
                    'message' => $message,
                ], $statusCode, $headers);
            default:
                $data = [
                    'error'   => true,
                    'message' => 'Something went wrong',
                ];
                $statusCode = method_exists($exception, 'getStatusCode') ? $exception->getStatusCode() : 500;

                return $this->responder($data, $statusCode);
        }
    }

    private function responder ($data, $statusCode, array $headers = []) {
        return app(ApiController::class)->respondError($data, $statusCode, $headers);
    }

    private function parseValidationErrorResponse (Exception $exception) {
        $errors = [];
        $statusCode = 422;
        if ($exception instanceof ValidationException) {
            $errors = $exception->errors();
        } elseif ($exception instanceof FormValidationException) {
            $errors = $exception->getResponse();
        }
        $chopped = [];
        foreach ( $errors as $key => $error ) {
            $chopped[$key] = $error[0];
        }

        return $this->responder([ 'error' => true, 'causes' => $chopped ], $statusCode);
    }
}
