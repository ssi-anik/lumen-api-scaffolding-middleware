<?php

namespace App\Jobs;

class SendEmailJob extends BaseJob
{
    private $mail;

    public function __construct ($mail, $tag = 'send-email') {
        $this->queue = 'email';
        $this->tries = 5;
        $this->tag = $tag;
        $this->mail = $mail;
    }

    public function handle () {
        $this->logger([ 'attempt' => $this->attempts() ]);

        app('mailer')->send($this->mail);

        $this->logger([ 'attempt' => $this->attempts(), 'deleting' => true ]);
        $this->delete();
    }
}
