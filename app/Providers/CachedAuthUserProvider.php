<?php

namespace App\Providers;

use App\Services\CacheService;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;

class CachedAuthUserProvider implements UserProvider
{
    private $provider, $cache;

    public function __construct (EloquentUserProvider $provider, CacheService $cache) {
        $this->provider = $provider;
        $this->cache = $cache;
    }

    public function retrieveById ($identifier) {
        $user = $this->cache->getAuthUser($identifier);

        if (!$user) {
            if ($user = $this->provider->retrieveById($identifier)) {
                $this->cache->setAuthUser($identifier, $user, config('settings.ttl.auth_user'));
            }
        }

        return $user;
    }

    public function retrieveByToken ($identifier, $token) {
        return $this->provider->retrieveByToken($identifier, $token);
    }

    public function updateRememberToken (Authenticatable $user, $token) {
        return $this->provider->updateRememberToken($user, $token);
    }

    public function retrieveByCredentials (array $credentials) {
        return $this->provider->retrieveByCredentials($credentials);
    }

    public function validateCredentials (Authenticatable $user, array $credentials) {
        return $this->provider->validateCredentials($user, $credentials);
    }
}