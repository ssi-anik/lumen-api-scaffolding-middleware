<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class QueryLoggerServiceProvider extends ServiceProvider
{
    public function register () {
        $this->app['db']->connection()->setEventDispatcher($this->app['events']);
    }
}
