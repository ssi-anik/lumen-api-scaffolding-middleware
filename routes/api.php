<?php

/** @var \Laravel\Lumen\Routing\Router $router */
$router->get('ping', 'HealthController@ping');
$router->get('health', 'HealthController@health');

// Prefixed with `/v1/`, namespace `Api\V1`
$router->group([ 'prefix' => 'v1', 'namespace' => 'V1', ], function () use ($router) {
    // auth routes
    $router->group([ 'prefix' => 'auth' ], function () use ($router) {
        $router->post('login', function () {
            return 'auth@login';
        });
        $router->post('login/2fa', function () {
            return 'auth@2falogin';
        });

        $router->group([ 'middleware' => 'auth', ], function () use ($router) {
            $router->post('refresh', function () {
                return 'auth@refresh';
            });
            $router->get('logout', function () {
                return 'auth@logout';
            });
        });
    });

    // routes requires authentication
    $router->group([ 'middleware' => [ 'auth' ] ], function () use ($router) {
        $router->get('/', function () {
            return 'home@auth';
        });
    });
});

$router->group([ 'middleware' => 'internal:developer,local,staging' ], function () use ($router) {
    $router->get('internal-middleware', function () {
        return [
            'message' => '`developer` key exists in `settings.api_keys.developer`. available for `local,staging` environments.',
        ];
    });
});